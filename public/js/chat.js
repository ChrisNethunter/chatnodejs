const socket = io();

//DOM ELEMENTS
let messsage = document.getElementById('message-to-send');
let username = document.getElementById('username');
let btnSubmitMessage = document.getElementById('send');
let output = document.getElementById('output');
let actions = document.getElementById('actions');


btnSubmitMessage.addEventListener( 'click' ,  ( event ) => {

    socket.emit('chat:message', {
        message : messsage.value,
        username : username.value
    });
    
});

messsage.addEventListener( 'keypress' ,  ( event ) => {

    socket.emit('chat:typing',username.value);
    
});


socket.on('chat:message', ( data ) => {
    output.innerHTML +=`
        <p>
            <strong> ${ data.username } </ strong> : ${ data.message }
        </p>

    ` 
});


socket.on('chat:typing', ( data ) => {
    actions.innerHTML = '';
    actions.innerHTML +=`
        <p>
            <strong> ${ data } </ strong> typing....
        </p>

    ` 
});