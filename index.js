const path = require('path');
const express = require('express');
const app = express();
var reload = require('reload');
var server
//settings
app.set('port', process.env.PORT || 3000)

console.log(__dirname)

app.use(express.static(path.join(__dirname, 'public')));




// Reload code here
reload(app).then(function (reloadReturned) {
    // reloadReturned is documented in the returns API in the README

    // Reload started, start web server
    //start server
    server  = app.listen( app.get('port') , () => {

        console.log(`server on port ${ app.get('port') }`)

    });

    const SocketIO = require('socket.io');

    const io = SocketIO.listen(server);
    // websocket

    io.on('connection', (socket) => {
        console.log('new connection', socket.id);
        
        socket.on('chat:message' , ( data ) => {
   
            io.sockets.emit('chat:message' , data );
        });
        

        socket.on('chat:typing' , ( username ) => {
           
            socket.broadcast.emit('chat:typing' , username );
        });
    });

    
}).catch(function (err) {
    console.error('Reload could not start, could not start server/sample app', err)
})



























/* //El siguiente código es un ejemplo de servidor web escrito en Node.js.



const http = require('http');
const hostname = '127.0.0.1';
const port = 3000;


const server = http.createServer((error , response ) => {
    response.statusCode = 200;
    response.setHeader('Content-Type', 'text/plain');

    response.end('Hola Mundo Valen algo mas\n');
});

server.listen(port, hostname, () => {

    console.log(`Server running at http://${hostname}:${port}/`);

});  */